package at.kara.vierGewinnt;

import java.util.Scanner;

public class Game {

	String[][] field = new String[7][6];

	boolean player1 = true;
	Scanner scan = new Scanner(System.in);
	boolean some1Won = false;
	boolean onOff = true;
	int a,b,c,d,e,f,g = 0;
	
	public void printField() {
		// TODO Auto-generated method stub
		System.out.println(field[0][5] + "|" + field[1][5] + "|" + field[2][5] + "|" + field[3][5] + "|" + field[4][5] + "|" + field[5][5] + "|" + field[6][5]);
		System.out.println(field[0][4] + "|" + field[1][4] + "|" + field[2][4] + "|" + field[3][4] + "|" + field[4][4] + "|" + field[5][4] + "|" + field[6][4]);
		System.out.println(field[0][3] + "|" + field[1][3] + "|" + field[2][3] + "|" + field[3][3] + "|" + field[4][3] + "|" + field[5][3] + "|" + field[6][3]);
		System.out.println(field[0][2] + "|" + field[1][2] + "|" + field[2][2] + "|" + field[3][2] + "|" + field[4][2] + "|" + field[5][2] + "|" + field[6][2]);
		System.out.println(field[0][1] + "|" + field[1][1] + "|" + field[2][1] + "|" + field[3][2] + "|" + field[4][1] + "|" + field[5][1] + "|" + field[6][1]);
		System.out.println(field[0][0] + "|" + field[1][0] + "|" + field[2][0] + "|" + field[3][0] + "|" + field[4][0] + "|" + field[5][0] + "|" + field[6][0]);
		
	}
	
	public void initField() {
		// TODO Auto-generated method stub
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 7; col++) {
				field[col][row] = " ";
			}
		}
	}
	
	public void setSymbol(int s, boolean player1) {

			switch (s) {
			case 0:
				if(player1 == true) {
					field[s][a] = "x";
				}
				else {
					field[s][a] = "o";
					
				}
				a++;
				break;
				
			case 1:
				if(player1 == true) {
					field[s][b] = "x";
				}
				else {
					field[s][b] = "o";
					
				}
				b++;
				break;
				
			case 2:
				if(player1 == true) {
					field[s][c] = "x";
				}
				else {
					field[s][c] = "o";
					
				}
				c++;
				break;
				
			case 3:
				if(player1 == true) {
					field[s][d] = "x";
				}
				else {
					field[s][d] = "o";
					
				}
				d++;
				break;
			
				
			case 4:
				if(player1 == true) {
					field[s][e] = "x";
				}
				else {
					field[s][e] = "o";
					
				}
				e++;
				break;
			case 5:
				if(player1 == true) {
					field[s][f] = "x";
				}
				else {
					field[s][f] = "o";
					
				}
				f++;
				break;
				
			case 6:
				if(player1 == true) {
					field[s][g] = "x";
				}
				else {
					field[s][g] = "o";
					
				}
				g++;
				break;
			}
	}
	
	public void checkFW() {
		
	}
	
	public void userInput() {
		if (player1 == true) {
			System.out.println("Enter your column Player 1 ");
			int s = scan.nextInt();
			if (field[s][5] == " ") {
				setSymbol(s, player1);
			} else {
				System.out.println("This place isn't empty");
				userInput();
			}
		} else {
			System.out.println("Enter your column Player 2 ");
			int s = scan.nextInt();
			if (field[s][5] == " ") {
				setSymbol(s, player1);
			} else {
				System.out.println("This place isn't empty");
				userInput();
			}
		}

	}
	
	public void game() {
		initField();
		printField();
		while (onOff) {
			userInput();
			printField();
			player1 = !player1;
		}

	}

}
