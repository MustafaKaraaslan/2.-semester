package at.kara.ticTacToe;

import java.util.Scanner;

public class Game {

	String[][] field = new String[3][3];

	boolean player1 = true;
	Scanner scan = new Scanner(System.in);
	boolean some1Won = false;
	boolean onOff = true;
	
	public void printField() {
		// TODO Auto-generated method stub
		System.out.println(field[0][0] + "|" + field[0][1] + "|" + field[0][2]);
		System.out.println(field[1][0] + "|" + field[1][1] + "|" + field[1][2]);
		System.out.println(field[2][0] + "|" + field[2][1] + "|" + field[2][2]);
	}

	public void initField() {
		// TODO Auto-generated method stub
		for (int col = 0; col < 3; col++) {
			for (int row = 0; row < 3; row++) {
				field[row][col] = " ";
			}
		}
	}

	public void setSymbol(int x, int y, boolean player1) {

		if (player1 == true) {
			field[x][y] = "x";
		} else {
			field[x][y] = "0";
		}

	}

	public void userInput() {
		if (player1 == true) {
			System.out.println("Enter your coordinations Player 1 ");
			String input = scan.next();
			String[] inputAr = input.split(",");
			int x = Integer.parseInt(inputAr[0]);
			int y = Integer.parseInt(inputAr[1]);
			if (field[x][y] == " ") {
				setSymbol(x, y, player1);
			} else {
				System.out.println("This place isn't empty");
				userInput();
			}
		} else {
			System.out.println("Enter your coordinations Player 2 ");
			String input = scan.next();
			String[] inputAr = input.split(",");
			int x = Integer.parseInt(inputAr[0]);
			int y = Integer.parseInt(inputAr[1]);
			if (field[x][y] == " ") {
				setSymbol(x, y, player1);
			} else {
				System.out.println("This place isn't empty");
				userInput();
			}
		}

	}
	
	public void checkFW() {
		
		if(field[0][0] == field[0][1] && field[0][1] == field[0][2] && field[0][0] != " ") {
			some1Won = true;
		}
		else if(field[1][0] == field[1][1] && field[1][1] == field[1][2] && field[1][0] != " ") {
			some1Won = true;
		}
		else if(field[2][0] == field[2][1] && field[2][1] == field[2][2] && field[2][0] != " ") {
			some1Won = true;
		}
		else if(field[0][0] == field[1][0] && field[1][0] == field[2][0] && field[0][0] != " ") {
			some1Won = true;
		}
		else if(field[0][1] == field[1][1] && field[1][1] == field[2][1] && field[0][1] != " ") {
			some1Won = true;
		}
		else if(field[0][2] == field[1][2] && field[1][2] == field[2][2] && field[0][2] != " ") {
			some1Won = true;
		}
		else if(field[0][0] == field[1][1] && field[1][1] == field[2][2] && field[0][0] != " ") {
			some1Won = true;
		}
		else if(field[2][0] == field[1][1] && field[1][1] == field[0][2] && field[2][0] != " ") {
			some1Won = true;
		}
	}

	public void game() {
		initField();
		printField();
		while (onOff) {
			userInput();
			checkFW();
			printField();
			if(some1Won) {
				if (player1) {
					System.out.println("Player 1 won");
				} else {
					System.out.println("Player 2 won");
				}
				onOff = false;
			}
			player1 = !player1;
		}

	}

}
