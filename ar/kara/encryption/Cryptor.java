package ar.kara.encryption;

public interface Cryptor  {

	public String Decrypt(String decrypt);
	public String Encrypt(String encrypt);
}
