package ar.kara.encryption;

public class CaesarEncryption implements Cryptor {

	private int shift;
	
	public CaesarEncryption(int shift) {
		super();
		this.shift = shift;
	}
	
	public CaesarEncryption() {
		super();
		this.shift = 2;
	}


	@Override
	public String Decrypt(String decrypt) {
		char[] decryptArray = decrypt.toCharArray();
		// TODO Auto-generated method stub
		String result = "";
		char c;
		for(int i = 0; i < decrypt.length(); i++) {
		 c = decryptArray[i];
		 int a = (int) c;
			if(97 <= a && a <= 122) {
				
				int shifted = a -= shift;
				
				if(shifted < 97) {
					int g = 97 - shifted;
					int neu = 123 - g;
					result += (char)neu;	
				}
				else {
					result += (char)shifted;
				}
			}
		}
		System.out.println(result + " wurde dechiffriert");
		return result;
	}

	@Override
	public String Encrypt(String encrypt) {
		char[] encryptArray = encrypt.toCharArray();
		// TODO Auto-generated method stub

		String result = "";
		char c;
		for(int i = 0; i < encrypt.length(); i++) {
		 c = encryptArray[i];
		 int a = (int) c;
			if(97 <= a && a <= 122) {
				
				int shifted = a += shift;
				
				if(shifted > 122) {
					int g = shifted - 122;
					int neu = g + 96;
					result += (char)neu;	
				}
				else {
					result += (char)shifted;
				}
			}

		}
		System.out.println(result + " wurde chiffriert");
		return result;
	}
	
	
	
	

}
